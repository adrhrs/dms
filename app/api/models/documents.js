const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true)
var mongoosePaginate = require('mongoose-paginate-v2');
var aggregatePaginate = require('mongoose-aggregate-paginate-v2');


const Schema = mongoose.Schema;

const documentschema = new Schema({
	
	number:  String,
    title:  String,
    department: [],
    description: String,

    categories:  [],
    
    user: {
      name: String,
      email:  String
    },

    parties: [],

    important_dates: [{
        name: String,
        date: Date
    }],

    files:[{
        filename: String,
        originalname: String,
        path: String,
        mimetype: String,
        size: Number
    }],

    creation_date: { type: Date, default: Date.now },


	
});

documentschema.plugin(mongoosePaginate);
documentschema.plugin(aggregatePaginate);

documentschema.index({'$**': 'text'});


module.exports = mongoose.model('Document', documentschema)