const documentModel = require('../models/documents');
const userModel = require('../models/users');



module.exports = {

    getById: function(req, res, next) {
        console.log(req.body);
        documentModel.findById(req.params.documentId, function(err, documentInfo) {
            if (err) {
                next(err);
            } else {
                res.json({
                    status: "success",
                    message: "document found!!!",
                    data: {
                        documents: documentInfo
                    }
                });
            }
        });
    },

    downloadDetail: function(req, res, next) {
        res.download('uploads/' + req.params.filename);
    },

    getAll: function(req, res, next) {

        // console.log(req.body)
        const myCustomLabels = {
            totalDocs: 'itemCount',
            docs: 'itemsList',
            limit: 'perPage',
            page: 'currentPage',
            nextPage: 'next',
            prevPage: 'prev',
            totalPages: 'pageCount',
            pagingCounter: 'slNo'
        }

        const options = {
            
            sort: {
                expired: 1
            },
            page: req.body.page,
            limit: 10,
            customLabels: myCustomLabels
        }

        var keyword = {}

        if(req.body.keyword !== "") {

            keyword = {$text: {$search: req.body.keyword}}

        }

        var aggregate = documentModel.aggregate()

        aggregate.match(keyword)
            // .project({_id:1})
            .addFields({
                expired: {
                    $arrayElemAt: ["$important_dates.date", 0]
                },
                effective: {
                    $arrayElemAt: ["$important_dates.date", 1]
                },
                agreement: {
                    $arrayElemAt: ["$important_dates.date", 2]
                },
                today: new Date(),
                datediff: {
                    $convert: {
                        input: {
                            $divide: [{
                                $subtract: [{
                                    $arrayElemAt: ["$important_dates.date", 0]
                                }, new Date()]
                            }, 86400000]
                        },
                        to: "int"
                    }
                }
            })



        documentModel.aggregatePaginate(aggregate, options, function(err, documents) {
            if (err) {
                next(err);
            } else {

                res.json({
                    status: "success",
                    message: "documents list found!!!",
                    data: {
                        documents: documents
                    }
                });

            }

        });
    },

    updateById: function(req, res, next) {

        userModel.findById(req.body.userId, function(err, userInfo) {

            documentModel.findByIdAndUpdate(req.params.documentId, {
                number: req.body.number,
                title: req.body.title,
                department: req.body.department,
                description: req.body.description,
                user: userInfo,

            }, function(err, result) {
                if (err)
                    next(err);
                else
                    res.json({
                        status: "success",
                        message: "document updated successfully!!!",
                        data: null
                    });

            });


        });

    },

    deleteById: function(req, res, next) {
        // console.log("delete")
        console.log(req.params.documentId)
        documentModel.findByIdAndRemove(req.params.documentId, function(err, documentInfo) {
            if (err)
                next(err);
            else {
                res.json({
                    status: "success",
                    message: "document deleted successfully!!!",
                    data: null
                });
            }
        });
    },


    create: function(req, res, next) {


        console.log(JSON.parse(req.body.data))

        var data = JSON.parse(req.body.data)

        userModel.findById(req.body.userId, function(err, userInfo) {

            documentModel.create({
                number: data.number,
                title: data.title,
                department: data.department_arr,
                important_dates: data.important_dates,
                description: data.description,
                categories: data.categories_arr,
                parties: data.parties_arr,
                files: data.files,
                user: userInfo,

            }, function(err, result) {
                if (err)
                    next(err);
                else
                    res.json({
                        status: "success",
                        message: "Document Saved Successfully",
                        data: null
                    });

            });


        });


    },

}