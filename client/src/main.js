import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Argon from "./plugins/argon-kit";
import './registerServiceWorker'

Vue.config.productionTip = false;
Vue.use(Argon)

window.backend = 'http://127.0.0.1:3000/';

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
