import Vue from "vue";
import Router from "vue-router";
import AppHeader from "./layout/AppHeader";
import AppFooter from "./layout/AppFooter";

import CreateDocs from "./views/CreateDocs.vue";
import Login from "./views/Login.vue";
import ListDocs from "./views/ListDocs.vue";

const loginguard = (to, from, next) => {
  
  if(!localStorage.getItem('jwt')) {
    next()
  } else {
    next(false)
  }
  
}

const auth = (to, from, next) => {
  
  if(!localStorage.getItem('jwt')) {

  
    next('/login')

  } else {
    next()
  }

  
}

const logout = (to, from, next) => {

  localStorage.clear()
  next('/login')

}

Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  mode: 'history',
  routes: [
   
    {
      path: '/logout',
      name: 'logout',
      beforeEnter: logout
    },
    {
      path: "/create",
      name: "CreateDocs",
      components: {
        header: AppHeader,
        default: CreateDocs,
        footer: AppFooter
      },
      beforeEnter: auth
    },

    {
      path: "/list",
      name: "ListDocs",
      components: {
        header: AppHeader,
        default: ListDocs,
        footer: AppFooter
      },
      beforeEnter: auth
    },

    {
      path: "/",
      name: "ListDocs",
      components: {
        header: AppHeader,
        default: ListDocs,
        footer: AppFooter
      },
      beforeEnter: auth
    },

    {
      path: "/login",
      name: "login",
      components: {
        
        default: Login,
        
      },
      beforeEnter: loginguard
    },
  
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
