const express = require('express');
const router = express.Router();
const documentController = require('../app/api/controllers/documents');

const multer = require('multer');
var path = require('path')

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function(req, file, cb) {
        cb(null, Math.random().toString(36).substr(2, 10) + path.extname(file.originalname))
    }
})

var upload = multer({
    storage: storage
})



router.post('/fetch', documentController.getAll);
router.post('/create', documentController.create);
router.get('/get/:documentId', documentController.getById);
router.post('/update/:documentId', documentController.updateById);
router.get('/delete/:documentId', documentController.deleteById);
router.get('/download/:filename', documentController.downloadDetail);
router.delete('/delete/:documentId', documentController.deleteById);
router.post('/upload', upload.any(), function(req, res, next) {

    const file = req.files
    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
    }

    res.json({
        status: "success",
        message: "docs uploaded",
        data: {
            documents: file
        }
    });

})

module.exports = router;